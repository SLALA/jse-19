package ru.t1.strelcov.tm.api.service;

import ru.t1.strelcov.tm.api.IService;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IService<Project> {

    List<Project> findAll(Comparator<Project> comparator);

    Project add(String name, String description);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project removeByName(String name);

    Project removeByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByName(String oldName, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeStatusById(String id, Status status);

    Project changeStatusByName(String oldName, Status status);

    Project changeStatusByIndex(Integer index, Status status);

}
