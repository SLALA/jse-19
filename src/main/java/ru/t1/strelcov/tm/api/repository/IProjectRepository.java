package ru.t1.strelcov.tm.api.repository;

import ru.t1.strelcov.tm.api.IRepository;
import ru.t1.strelcov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    List<Project> findAll(Comparator<Project> comparator);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project removeByName(String name);

    Project removeByIndex(Integer index);

}
