package ru.t1.strelcov.tm.api.repository;

import ru.t1.strelcov.tm.api.IRepository;
import ru.t1.strelcov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User removeByLogin(String login);

}
