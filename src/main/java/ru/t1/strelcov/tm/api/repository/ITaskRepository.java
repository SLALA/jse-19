package ru.t1.strelcov.tm.api.repository;

import ru.t1.strelcov.tm.api.IRepository;
import ru.t1.strelcov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAll(Comparator<Task> comparator);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    void removeAllByProjectId(String projectId);

    List<Task> findAllByProjectId(String projectId);

}
