package ru.t1.strelcov.tm.api.service;

import ru.t1.strelcov.tm.api.IService;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IService<Task> {

    List<Task> findAll(Comparator<Task> comparator);

    Task add(String name, String description);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByName(String oldName, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeStatusById(String id, Status status);

    Task changeStatusByName(String oldName, Status status);

    Task changeStatusByIndex(Integer index, Status status);

}
