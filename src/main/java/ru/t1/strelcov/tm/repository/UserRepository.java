package ru.t1.strelcov.tm.repository;

import ru.t1.strelcov.tm.api.repository.IUserRepository;
import ru.t1.strelcov.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        for (final User user : list) {
            if (login.equals(user.getLogin()))
                return user;
        }
        return null;
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        remove(user);
        return user;
    }

}
