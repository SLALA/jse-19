package ru.t1.strelcov.tm.repository;

import ru.t1.strelcov.tm.api.IRepository;
import ru.t1.strelcov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public void add(final E entity) {
        list.add(entity);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public void remove(final E entity) {
        list.remove(entity);
    }

    @Override
    public E findById(final String id) {
        for (final E entity : list) {
            if (id.equals(entity.getId()))
                return entity;
        }
        return null;
    }

    @Override
    public E removeById(final String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        remove(entity);
        return entity;
    }

}
