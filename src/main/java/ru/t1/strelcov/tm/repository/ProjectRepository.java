package ru.t1.strelcov.tm.repository;

import ru.t1.strelcov.tm.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        final List<Project> projects = new ArrayList<>(list);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public Project findByName(final String name) {
        for (final Project project : list) {
            if (name.equals(project.getName()))
                return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final Integer index) {
        if (index >= list.size()) return null;
        return list.get(index);
    }

    @Override
    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        remove(project);
        return project;
    }

}
