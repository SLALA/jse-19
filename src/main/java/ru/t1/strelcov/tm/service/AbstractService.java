package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.IRepository;
import ru.t1.strelcov.tm.api.IService;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.model.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public void add(final E entity) {
        if (entity == null) return;
        repository.add(entity);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) return;
        repository.remove(entity);
    }

    @Override
    public E findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = repository.findById(id);
        return entity;
    }

    @Override
    public E removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        E entity = findById(id);
        if (entity == null) throw new EntityNotFoundException();
        entity = repository.removeById(id);
        return entity;
    }

}
