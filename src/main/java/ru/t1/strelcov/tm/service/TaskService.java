package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.repository.ITaskRepository;
import ru.t1.strelcov.tm.api.service.ITaskService;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyNameException;
import ru.t1.strelcov.tm.exception.system.IncorrectIndexException;
import ru.t1.strelcov.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task add(final String name, final String description) {
        final Task task;
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty())
            task = new Task(name);
        else
            task = new Task(name, description);
        add(task);
        return task;
    }

    @Override
    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByName(name);
        return task;
    }

    @Override
    public Task findByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        final Task task = taskRepository.findByIndex(index);
        return task;
    }

    @Override
    public Task removeByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.removeByName(name);
        return task;
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        final Task task = taskRepository.removeByIndex(index);
        return task;
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        final Task task = findById(id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByName(final String oldName, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findByName(oldName);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS)
            task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task changeStatusByName(final String name, final Status status) {
        final Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS)
            task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS)
            task.setDateStart(new Date());
        return task;
    }

}
