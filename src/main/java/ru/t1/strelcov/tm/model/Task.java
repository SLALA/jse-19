package ru.t1.strelcov.tm.model;

import ru.t1.strelcov.tm.api.entity.IWBS;
import ru.t1.strelcov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

import static ru.t1.strelcov.tm.enumerated.Status.*;

public final class Task extends AbstractEntity implements IWBS {

    private String name = "";

    private String description = "";

    private Status status = NOT_STARTED;

    private String projectId;

    private Date created = new Date();

    private Date dateStart;

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }


    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return id + " : " + name + " : " + created.toString();
    }

}
