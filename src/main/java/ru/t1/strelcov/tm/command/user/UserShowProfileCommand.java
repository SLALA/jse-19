package ru.t1.strelcov.tm.command.user;

import ru.t1.strelcov.tm.api.service.IAuthService;
import ru.t1.strelcov.tm.model.User;

public final class UserShowProfileCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-profile";
    }

    @Override
    public String description() {
        return "Display the current user profile data.";
    }

    @Override
    public void execute() {
        final IAuthService authService = serviceLocator.getAuthService();
        System.out.println("[PROFILE]");
        final User user = authService.getUser();
        showUser(user);
    }

}
