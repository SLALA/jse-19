package ru.t1.strelcov.tm.command.user;

import ru.t1.strelcov.tm.api.service.IAuthService;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "login";
    }

    @Override
    public String description() {
        return "Login.";
    }

    @Override
    public void execute() {
        final IAuthService authService = serviceLocator.getAuthService();
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        authService.login(login, password);
    }

}
